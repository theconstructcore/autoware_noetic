#!/usr/bin/env python
# -*- coding: UTF-8 -*-

import cv2

import rospy
from sensor_msgs.msg import Image
from std_msgs.msg import Header
from cv_bridge import CvBridge, CvBridgeError
from autoware_msgs.msg import image_obj


class AutowareVehiclePedestrianDetector():
    def __init__(self, rate_publish=5.0):

        self.rate = rospy.Rate(rate_publish)

        self.bridge_object = CvBridge()
        self.car_detections_topic = "/obj_car/image_obj"
        self.ped_detections_topic = "/obj_person/image_obj"
        
        self.cam_topic = "/rgb_camera/image_raw"
        self.detections_image_publisher = "/obj_car/image"

        self._check_cam_ready()
        self._check_car_detections_ready()
        self._check_ped_detections_ready()

        self.image_sub = rospy.Subscriber(self.cam_topic,Image,self.camera_callback)
        car_sub_image = rospy.Subscriber(self.car_detections_topic, image_obj, self.car_detection_callback, queue_size=1)
        ped_sub_image = rospy.Subscriber(self.ped_detections_topic, image_obj, self.pedestrian_detection_callback, queue_size=1)
        
        self.pub_image = rospy.Publisher(self.detections_image_publisher, Image, queue_size=1)
       
        rospy.loginfo("Finished Init process...Ready")

    def _check_cam_ready(self):
      self.cam_image = None
      while self.cam_image is None and not rospy.is_shutdown():
         try:
               self.cam_image = rospy.wait_for_message(self.cam_topic, Image, timeout=1.0)
               rospy.logdebug("Current "+self.cam_topic+" READY=>" + str(self.cam_image))

         except:
               rospy.logerr("Current "+self.cam_topic+" not ready yet, retrying.")

    def _check_car_detections_ready(self):
      self.car_detection_msg = None
      while self.car_detection_msg is None and not rospy.is_shutdown():
         try:
               self.car_detection_msg = rospy.wait_for_message(self.car_detections_topic, image_obj, timeout=1.0)
               rospy.logdebug("Current "+self.car_detections_topic+" READY=>" + str(self.car_detection_msg))

         except:
               rospy.logerr("Current "+self.car_detections_topic+" not ready yet, retrying.")

    def _check_ped_detections_ready(self):
      self.pedestrian_detection_msg = None
      while self.pedestrian_detection_msg is None and not rospy.is_shutdown():
         try:
               self.pedestrian_detection_msg = rospy.wait_for_message(self.ped_detections_topic, image_obj, timeout=1.0)
               rospy.logdebug("Current "+self.ped_detections_topic+" READY=>" + str(self.pedestrian_detection_msg))

         except:
               rospy.logerr("Current "+self.ped_detections_topic+" not ready yet, retrying.")
    

    def camera_callback(self,data):
        self.cam_image = data


    def car_detection_callback(self, data):        
        self.car_detection_msg = data

    
    def pedestrian_detection_callback(self, data):        
        self.pedestrian_detection_msg = data

    def publish_detection_image(self):

        self.cam_image, self.pedestrian_detection_msg, self.car_detection_msg
        # We get the Camera being published
        try:
            # We select bgr8 because its the OpneCV encoding by default
            video_capture = self.bridge_object.imgmsg_to_cv2(self.cam_image, desired_encoding="bgr8")
        except CvBridgeError as e:
            print(e)

        car_detect_type, car_detect_obj = self.extract_detection_info(self.car_detection_msg)
        video_capture_with_cars = self.place_data_in_image(video_capture, car_detect_obj, car_detect_type)
        
        pedestrian_detect_type, pedestrian_detect_obj = self.extract_detection_info(self.pedestrian_detection_msg)
        video_capture_with_cars_ped = self.place_data_in_image(video_capture_with_cars, pedestrian_detect_obj, pedestrian_detect_type)

        # Display the resulting image
        cv2.imshow("Image window", video_capture_with_cars_ped)
        cv2.waitKey(1)

    def place_data_in_image(self, in_image, detection_obj, detection_type):
        # Draw a box around the face
        for detected_element in detection_obj:
            left = int(detected_element.x)
            right = int(detected_element.x + detected_element.width)
            top = int(detected_element.y )
            bottom = int(detected_element.y + detected_element.height )

            detect_perc = int(abs(detected_element.score * 100))

            rospy.loginfo("left="+str(left))
            rospy.loginfo("right="+str(right))
            rospy.loginfo("top="+str(top))
            rospy.loginfo("bottom="+str(bottom))
            rospy.loginfo("detect_perc="+str(detect_perc))

            if detection_type == "car":
                rect_colour = (0, 0, 255)
            elif detection_type == "person":
                rect_colour = (255, 0, 0)
            else: 
                rect_colour = (255, 255, 255)

            cv2.rectangle(in_image, (left, top), (right, bottom), rect_colour, 2)

            # Draw a label with a name below the face
            cv2.rectangle(in_image, (left, bottom - 35), (right, bottom), rect_colour)
            font = cv2.FONT_HERSHEY_DUPLEX
            cv2.putText(in_image, detection_type, (left + 6, bottom - 6), font, 1.0, (255, 255, 255), 1)
            if detect_perc > 50:
                cv2.putText(in_image, str(detect_perc), (right - 60, bottom - 6), font, 1.0, (0, 255, 0), 1)
            else:
                cv2.putText(in_image, str(detect_perc), (right - 60, bottom - 6), font, 1.0, (0, 0, 255), 1)
        
        return in_image
        

    def extract_detection_info(self, detect_data):
        """
        header: 
            seq: 108
            stamp: 
                secs: 90
                nsecs: 206000000
            frame_id: "rgb_camera_camera_link_frame"
        type: "car"
        obj: (ITS A LIST)
        - 
            x: 320
            y: 215
            height: 138
            width: 163
            score: 1.254780650138855

        """

        car_detect_type = detect_data.type
        car_detect_obj = detect_data.obj

        return car_detect_type, car_detect_obj

    def loop(self):

        while not rospy.is_shutdown():
            self.publish_detection_image()
            self.rate.sleep()


def main():
    rospy.init_node('object_detection_python_node', anonymous=True, log_level=rospy.INFO)

    face_recogniser_object = AutowareVehiclePedestrianDetector()

    face_recogniser_object.loop()
    cv2.destroyAllWindows()


if __name__ == '__main__':
    main()