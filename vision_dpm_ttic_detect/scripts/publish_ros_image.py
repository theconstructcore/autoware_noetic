#!/usr/bin/env python
# -*- coding: UTF-8 -*-

import cv2

import rospy
from sensor_msgs.msg import Image
from std_msgs.msg import Header
from cv_bridge import CvBridge, CvBridgeError
from autoware_msgs.msg import image_obj


class AutowareVehiclePedestrianDetector():
    def __init__(self):

        self.bridge_object = CvBridge()
        self.detections_topic = "/obj_car/image_obj"
        self.detections_topic = "/obj_person/image_obj"
        
        self.cam_topic = "/rgb_camera/image_raw"
        self.detections_image_publisher = "/obj_car/image"

        self._check_cam_ready()
        self._check_detections_ready()

        self.image_sub = rospy.Subscriber(self.cam_topic,Image,self.camera_callback)
        sub_image = rospy.Subscriber(self.detections_topic, image_obj, self.detection_callback, queue_size=1)
        
        self.pub_image = rospy.Publisher(self.detections_image_publisher, Image, queue_size=1)

       
        rospy.loginfo("Finished Init process...Ready")

    def _check_cam_ready(self):
      self.cam_image = None
      while self.cam_image is None and not rospy.is_shutdown():
         try:
               self.cam_image = rospy.wait_for_message(self.cam_topic, Image, timeout=1.0)
               rospy.logdebug("Current "+self.cam_topic+" READY=>" + str(self.cam_image))

         except:
               rospy.logerr("Current "+self.cam_topic+" not ready yet, retrying.")

    def _check_detections_ready(self):
      self.detection_data = None
      while self.detection_data is None and not rospy.is_shutdown():
         try:
               self.detection_data = rospy.wait_for_message(self.detections_topic, image_obj, timeout=1.0)
               rospy.logdebug("Current "+self.detections_topic+" READY=>" + str(self.detection_data))

         except:
               rospy.logerr("Current "+self.detections_topic+" not ready yet, retrying.")
    

    def camera_callback(self,data):
        self.cam_image = data

    def detection_callback(self, detect_data):
        # We get the Camera being published
        try:
            # We select bgr8 because its the OpneCV encoding by default
            video_capture = self.bridge_object.imgmsg_to_cv2(self.cam_image, desired_encoding="bgr8")
        except CvBridgeError as e:
            print(e)

        detect_type, detect_obj = self.extract_detection_info(detect_data)

        # Draw a box around the face
        for detected_element in detect_obj:
            left = int(detected_element.x)
            right = int(detected_element.x + detected_element.width)
            top = int(detected_element.y )
            bottom = int(detected_element.y + detected_element.height )

            detect_perc = int(detected_element.score * 100)

            rospy.loginfo("left="+str(left))
            rospy.loginfo("right="+str(right))
            rospy.loginfo("top="+str(top))
            rospy.loginfo("bottom="+str(bottom))
            rospy.loginfo("detect_perc="+str(detect_perc))


            cv2.rectangle(video_capture, (left, top), (right, bottom), (0, 0, 255), 2)

            # Draw a label with a name below the face
            cv2.rectangle(video_capture, (left, bottom - 35), (right, bottom), (0, 0, 255))
            font = cv2.FONT_HERSHEY_DUPLEX
            cv2.putText(video_capture, detect_type, (left + 6, bottom - 6), font, 1.0, (255, 255, 255), 1)
            if detect_perc > 50:
                cv2.putText(video_capture, str(detect_perc), (right - 60, bottom - 6), font, 1.0, (0, 255, 0), 1)
            else:
                cv2.putText(video_capture, str(detect_perc), (right - 60, bottom - 6), font, 1.0, (0, 0, 255), 1)

        # Display the resulting image
        cv2.imshow("Image window", video_capture)
        cv2.waitKey(1)


        

    def extract_detection_info(self, detect_data):
        """
        header: 
            seq: 108
            stamp: 
                secs: 90
                nsecs: 206000000
            frame_id: "rgb_camera_camera_link_frame"
        type: "car"
        obj: (ITS A LIST)
        - 
            x: 320
            y: 215
            height: 138
            width: 163
            score: 1.254780650138855

        """

        detect_type = detect_data.type
        detect_obj = detect_data.obj

        return detect_type, detect_obj


def main():
    rospy.init_node('object_detection_python_node', anonymous=True, log_level=rospy.INFO)

    face_recogniser_object = AutowareVehiclePedestrianDetector()

    rospy.spin()
    cv2.destroyAllWindows()


if __name__ == '__main__':
    main()