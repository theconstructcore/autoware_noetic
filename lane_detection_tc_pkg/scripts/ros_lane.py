#!/usr/bin/env python
import sys
import os
import rospy
from sensor_msgs.msg import Image
import rospkg
from lane import Lane
import cv2
from cv_bridge import CvBridge, CvBridgeError

from image_publisher import ImagePublisher
from lane_detection_tc_pkg.msg import LaneData


class ROSLane(object):

    def __init__(self, camera_topic = "/rgb_camera/image_raw", lanet_image_topic = "/lane/image_raw", lane_data_topic="/lane/data"):
        rospy.loginfo("Start Lane Detector Init process...")
        self._lanet_image_topic = lanet_image_topic
        self._lane_data_topic = lane_data_topic
        # get an instance of RosPack with the default search paths
        self.rate = rospy.Rate(5)

        self.bridge_object = CvBridge()

        self.cam_topic = camera_topic
        self._check_cam_ready()
        self.image_sub = rospy.Subscriber(self.cam_topic,Image,self.camera_callback)

        # Init Image Detection Publisher
        
        self.img_publisher_obj = ImagePublisher( encoding=self.cam_img_check.encoding, 
                                        frame_id = self.cam_img_check.header.frame_id,
                                        output_image_topic = '/lane_detections/image_raw')

        # Lane data Publisher
        self.lane_data_pub = rospy.Publisher(self._lane_data_topic, LaneData, queue_size=1)
        self.lane_data_msg = LaneData()
        self.lane_data_msg.valid_lane = False

        # Create a Lane object
        self.lane_object = Lane()
        self.lane_object.update_orig_frame(self.cam_image)
        self.lane_object.init_image_frame_data()


        rospy.loginfo("Finished Lane Detector Init process...Ready")

    def _check_cam_ready(self):
        cam_img_check = None
        while cam_img_check is None and not rospy.is_shutdown():
            try:
                cam_img_check = rospy.wait_for_message(self.cam_topic, Image, timeout=1.0)
                rospy.logdebug("Current "+self.cam_topic+" READY=>" + str(cam_img_check))

            except:
                rospy.logerr("Current "+self.cam_topic+" not ready yet, retrying.")
        self.cam_img_check = cam_img_check
        self.cam_image = self.rosimg_to_opencv(self.cam_img_check)

    def rosimg_to_opencv(self,data):
        try:
            # We select bgr8 because its the OpenCV encoding by default
            opencv_img = self.bridge_object.imgmsg_to_cv2(data, desired_encoding="bgr8")
        except CvBridgeError as e:
            print(e)

        return opencv_img

    def camera_callback(self,data):
        self.cam_image = self.rosimg_to_opencv(data)

    def loop(self):

        while not rospy.is_shutdown():
            self.recognise()
            self.rate.sleep()

    def recognise(self):
        rospy.loginfo("Recognise...")
        frame_with_lane_lines2 = self.lane_processing()
        self.img_publisher_obj.publish(frame_with_lane_lines2)
        self.lane_data_pub.publish(self.lane_data_msg)

    
    def lane_processing(self):

        try:
            self.lane_object.update_orig_frame(self.cam_image)      
            
            # Perform thresholding to isolate lane lines
            lane_line_markings = self.lane_object.get_line_markings()
            
            # Plot the region of interest on the image
            self.lane_object.plot_roi(plot=False)
            
            # Perform the perspective transform to generate a bird's eye view
            # If Plot == True, show image with new region of interest
            warped_frame = self.lane_object.perspective_transform(plot=False)
            
            # Generate the image histogram to serve as a starting point
            # for finding lane line pixels
            histogram = self.lane_object.calculate_histogram(plot=False)  
                
            # Find lane line pixels using the sliding window method 
            left_fit, right_fit = self.lane_object.get_lane_line_indices_sliding_windows(
                plot=False)
            
            # Fill in the lane line
            self.lane_object.get_lane_line_previous_window(left_fit, right_fit, plot=False)
                
            # Overlay lines on the original frame
            frame_with_lane_lines = self.lane_object.overlay_lane_lines(plot=False)
            
            # Calculate lane line curvature (left and right lane lines)
            self.lane_object.calculate_curvature(print_to_terminal=False)
            
            # Calculate center offset                                                                 
            self.lane_object.calculate_car_position(print_to_terminal=False)
                
            # Display curvature and center offset on image
            frame_with_lane_lines2 = self.lane_object.display_curvature_offset(
                frame=frame_with_lane_lines, plot=False)

            # We Update the Lane data Object
            curve_radius, center_offset = self.lane_object.get_lane_data()

            self.lane_data_msg.curve_radius = curve_radius
            self.lane_data_msg.center_offset = center_offset
            self.lane_data_msg.valid_lane = True

        except TypeError:
            rospy.logerr("Lanes LOST!!!")
            x_coord = int(self.lane_object.width / 2)
            y_coord = int(self.lane_object.height / 2)
            font = cv2.FONT_HERSHEY_DUPLEX
            cv2.putText(self.cam_image, "LOST LANES!", (x_coord , y_coord ), font, 1.0, (0, 0, 255), 2)
            frame_with_lane_lines2 = self.cam_image

            self.lane_data_msg.valid_lane = False
            
        #return frame_with_lane_lines2
        return frame_with_lane_lines2




def main():
    rospy.init_node('lane_detection_node', anonymous=True, log_level=rospy.INFO)

    ros_lane_obj = ROSLane()

    ros_lane_obj.loop()
    cv2.destroyAllWindows()


if __name__ == '__main__':
    main()