#!/usr/bin/env python
# -*- coding: utf-8 -*-
import os
from threading import Lock

import cv2
import numpy as np
from cv_bridge import CvBridge, CvBridgeError
from cv_bridge.boost.cv_bridge_boost import getCvType

import rospy
from sensor_msgs.msg import Image


class ImagePublisher(object):

    def __init__(self, encoding='bgr8', frame_id = 'camera', output_image_topic = '/image_publisher/image_raw'):
        self.lock = Lock()
        self._encoding = encoding
        self._frame_id = frame_id
        self.imgmsg = None
        self._output_image_topic = output_image_topic

        self.bridge_object = CvBridge()

        self.pub = rospy.Publisher(self._output_image_topic, Image, queue_size=1)
        
    def publish(self, cv2_image_obj):

        image_msg = self.cv2_to_imgmsg(img_bgr=cv2_image_obj, encoding=self._encoding)
        self.imgmsg = image_msg
        if self.imgmsg is None:
            return
        now = rospy.Time.now()
        # setup ros message and publish
        with self.lock:
            self.imgmsg.header.stamp = now
            self.imgmsg.header.frame_id = self._frame_id
        self.pub.publish(self.imgmsg)
        

    def cv2_to_imgmsg(self, img_bgr, encoding):
        bridge = CvBridge()
        # resolve encoding
        if getCvType(encoding) == 0:
            # mono8
            img = cv2.cvtColor(img_bgr, cv2.COLOR_BGR2GRAY)
        elif getCvType(encoding) == 2:
            # 16UC1
            img = cv2.cvtColor(img_bgr, cv2.COLOR_BGR2GRAY)
            img = img.astype(np.float32)
            img = img / 255 * (2 ** 16)
            img = img.astype(np.uint16)
        elif getCvType(encoding) == 5:
            # 32FC1
            img = cv2.cvtColor(img_bgr, cv2.COLOR_BGR2GRAY)
            img = img.astype(np.float32)
            img /= 255
        elif getCvType(encoding) == 16:
            # 8UC3
            if encoding in ('rgb8', 'rgb16'):
                # RGB
                img = cv2.cvtColor(img_bgr, cv2.COLOR_BGR2RGB)
            else:
                img = img_bgr
        else:
            rospy.logerr('unsupported encoding: {0}'.format(encoding))
            return
        return bridge.cv2_to_imgmsg(img, encoding=encoding)

    def rosimg_to_opencv(self, data):
        try:
            # We select bgr8 because its the OpenCV encoding by default
            opencv_img = self.bridge_object.imgmsg_to_cv2(data, desired_encoding="bgr8")
        except CvBridgeError as e:
            print(e)

        return opencv_img

def get_cam_image(cam_topic):
    cam_img_check = None
    while cam_img_check is None and not rospy.is_shutdown():
        try:
            cam_img_check = rospy.wait_for_message(cam_topic, Image, timeout=1.0)
            rospy.logdebug("Current "+cam_topic+" READY=>" + str(cam_img_check))

        except:
            rospy.logerr("Current "+cam_topic+" not ready yet, retrying.")
    
    return cam_img_check



def test():
    rospy.init_node('image_publisher')
    img_publisher_obj = ImagePublisher( encoding='bgr8', 
                                        frame_id = 'camera',
                                        output_image_topic = '/image_publisher/image_raw')
    cam_topic = "/rgb_camera/image_raw"
    rate = rospy.Rate(24)
    while not rospy.is_shutdown():
        raw_image = get_cam_image(cam_topic)
        cv2_image_obj = img_publisher_obj.rosimg_to_opencv(raw_image)
        img_publisher_obj.publish(cv2_image_obj)
        rate.sleep()


if __name__ == '__main__':
    test()
